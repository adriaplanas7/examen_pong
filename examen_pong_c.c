/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"


typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, MULTIPLAYER_GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "STAR PONG";
   
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    GameScreen screen = LOGO;
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    
    //LOGO 
    float alpha = 0.0f;
    bool fadeIn = true;
        
    //TITLE VARIABLES
    
    char titleText[20] = "STAR PONG";
    char startText1[20] = "PRESS ENTER!";
    char startText2[20] = "[M] MULTIPLAYER";
    int titleAnimation= -100;
    
    bool blink = false;
    
    //GAMEPLAY VARIABLES
    
    float topMargin = 50;
    float bgMargin = 5;
    float fieldMargin = 10;
    float counterMargin = 35;
    float lifeMargin = 5 ;
    
    
    
    Rectangle bgRec = {0, topMargin, screenWidth, screenHeight - topMargin};
    Rectangle fieldRec  = {bgRec.x + bgMargin, bgRec.y + bgMargin, bgRec.width - bgMargin*2,bgRec.height -bgMargin*2};
    //white
    
    //LIFE BARS
    
    //PLAYER'S
    
    Rectangle playerBgRec = {0,0,screenWidth/2 - counterMargin,topMargin};
    Rectangle playerFillRec = {playerBgRec.x + lifeMargin, playerBgRec.y + lifeMargin, playerBgRec.width - lifeMargin*2, playerBgRec.height - lifeMargin*2};
    Rectangle playerLifeRec = playerFillRec;
     
    
    //ENEMY'S
    
    Rectangle enemyBgRec = {screenWidth/2 + counterMargin,0,screenWidth/2 - lifeMargin,topMargin};
    Rectangle enemyFillRec = {enemyBgRec.x + lifeMargin, enemyBgRec.y + lifeMargin, enemyBgRec.width - lifeMargin*2, enemyBgRec.height - lifeMargin*2};
    Rectangle enemyLifeRec = enemyFillRec; 
       
    
    Rectangle player;       //PLAYER
    
    int playerSpeedY = 5;
     
    player.width = 10;
    player.height = 60;
    player.x = fieldRec.x + fieldMargin;
    player.y = fieldRec.y + fieldRec.height/2 - player.height/2; 
    
    
    Rectangle enemy;        //ENEMY
    
    int enemySpeedY = 5;
    
    
    enemy.width = 10;
    enemy.height = 60;
    enemy.x = fieldRec.x + fieldRec.width-fieldMargin-10;
    enemy.y = fieldRec.y + fieldRec.height/2 - enemy.height/2;
    
    float AImargin = enemy.height/4;
    
              //MID STAR (NEUTRAL)//
    
    bool alive1=true;
    bool alive2=true;
    bool alive3=true;
    bool alive4=true;
    
    
    int midContador1=0;
    int midContador2=0;
    int midContador3=0;
    int midContador4=0;
    
    
    Rectangle midstar1 = {fieldRec.x + fieldRec.width/2-100,fieldRec.y  - enemy.height,player.width,player.height};
    Rectangle midstar2 = {fieldRec.x + fieldRec.width/2-250,fieldRec.y  - enemy.height,player.width,player.height};
    Rectangle midstar3 = {fieldRec.x + fieldRec.width/2+250-player.width,fieldRec.y  - enemy.height,player.width,player.height};
    Rectangle midstar4 = {fieldRec.x + fieldRec.width/2+100-player.width,fieldRec.y  - enemy.height,player.width,player.height};
    
    Vector2 ballPosition = {fieldRec.x + fieldRec.width/2, fieldRec.y + fieldRec.height/2};
    Vector2 ballSpeed = {GetRandomValue(4,7), GetRandomValue(4,7)};                                                                   //BALL
    
    if(GetRandomValue(0,1)) ballSpeed.x *= -1;
    if(GetRandomValue(0,1)) ballSpeed.y *= -1;
    
    int ballRadius = 20;
    
    //COLORS
    
    Color playercolor = {255,252,64,255};
    Color player_life_color = {172,170,48,255};

    Color enemycolor = {187,0,255,255};
    Color enemy_life_color = {91,65,101,255};
    
    //Color ballcolor = {117,105,255,255};
    
    
    int playerLife=5;
    int enemyLife=5;
    
    float Damage = playerLifeRec.width/playerLife;
    
    int secondsCounter = 99;
    
    bool pause = false;
    
    int framesCounter=0;          // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, 2 - Draw, -1 - Not defined
    
    
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    
    Texture2D logo =  LoadTexture("resources/logo.png");
    Texture2D background =  LoadTexture("resources/background.png");
    Texture2D background2 =  LoadTexture("resources/background2.png");
    Texture2D ball =  LoadTexture("resources/ball.png");
    
    Texture2D background_star =  LoadTexture("resources/background_star.png");
    Texture2D background_dark =  LoadTexture("resources/background_dark.png");
    Texture2D background_draw =  LoadTexture("resources/background_draw.png");

        
        
    Texture2D darkstar_end =  LoadTexture("resources/darkstar_end.png");
    Texture2D playerstar_end =  LoadTexture("resources/playerstar_end.png");
    Texture2D drawstar_end =  LoadTexture("resources/drawstar_end.png");
    
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
      InitAudioDevice();      // Initialize audio device
    
    Music gameMusic = LoadMusicStream ("resources/gameMusic.ogg");
    
    Sound logo_music = LoadSound("resources/logo.wav");
    
    Music title_music = LoadMusicStream("resources/title_music.ogg");
    
    Sound star = LoadSound("resources/player.wav");
    Sound player_hurt = LoadSound("resources/player_hurt.wav");
    
    Sound darkstar = LoadSound("resources/enemy.wav");
    Sound enemy_hurt = LoadSound("resources/enemy_hurt.wav");
    Sound midstar = LoadSound("resources/midstar.wav");
     
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                            
                if(fadeIn){
                    
                    alpha+= 1.0f/90;
                    
                    
                    if(alpha >= 1.0f)
                    {
                        alpha = 1.0f;
                        
                        
                        framesCounter++;
                        
                            if(framesCounter % 120 == 0){
                            
                                framesCounter = 0;
                                fadeIn = false;
                                PlaySound (logo_music);
                        }
                    }
                    
                }
                else{
                    
                    alpha -= 1.0f/90;
                    
                    if (alpha<=0.0f){
                        framesCounter = 0;
                        screen = TITLE;
                    }
                }
                
                if(IsKeyPressed(KEY_ENTER))
                {
                    
                        framesCounter = 0;
                        screen = TITLE;
                    
                }
                
                
            } break;
            case TITLE: 
            UpdateMusicStream(title_music);
            PlayMusicStream(title_music);
            
            {
                // Update TITLE screen data here!
                
                // TODO: Title animation logic.......................(0.5p)
                
                
                if(titleAnimation<80){
                    titleAnimation += 5;
                    
                    if(IsKeyPressed(KEY_ENTER)){
                        
                        titleAnimation=80;
                    }
                }
                
                else{
                    
                    titleAnimation=80;
                    
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                framesCounter++;
                if(framesCounter % 15 == 0)
                    {
                    
                    framesCounter = 0;
                    blink = !blink; 
                    }
                    
                    if(IsKeyPressed(KEY_ENTER))
                    {
                        framesCounter = 0;
                        screen = GAMEPLAY;
                    }
                    
                    if(IsKeyPressed(KEY_M))
                    {
                        framesCounter = 0;
                        screen = MULTIPLAYER_GAMEPLAY;
                    }
                    
                }
                 
                
            }break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!

            if (!pause)
            {
                UpdateMusicStream(gameMusic);
                PlayMusicStream (gameMusic);
                
                     // TODO: Ball movement logic.........................(0.2p)
                
                ballPosition.x = ballPosition.x + ballSpeed.x;
                ballPosition.y = ballPosition.y + ballSpeed.y;
                
                // TODO: Player movement logic.......................(0.2p)
                
                    if (IsKeyDown(KEY_W)) player.y -= playerSpeedY;
                    if (IsKeyDown(KEY_S)) player.y += playerSpeedY;
                    
                    
                                
                    if(player.y<fieldRec.y)
                            {
                                player.y = fieldRec.y;
                            }
                            
                    else if(player.y>(fieldRec.y + fieldRec.height - player.height))
                            {
                                player.y = fieldRec.y + fieldRec.height - player.height;
                            }
                    
                    
                // TODO: Enemy movement logic (IA)...................(1p)
                
                if ((ballPosition.x > fieldRec.x + fieldRec.width/2) && ballSpeed.x > 0)
                {
                    if(ballPosition.y < enemy.y + enemy.height/2 - AImargin) enemy.y -= enemySpeedY;
                    if(ballPosition.y > enemy.y + enemy.height/2 + AImargin) enemy.y += enemySpeedY;
                }
                
                if(enemy.y<fieldRec.y)
                            {
                                enemy.y = fieldRec.y;
                            }   
                    else if(enemy.y>(fieldRec.y + fieldRec.height - enemy.height))
                            {
                                enemy.y = fieldRec.y + fieldRec.height - enemy.height;
                            }
                
                // Mid star movement
                
                
                midContador1++;
                midContador2++;
                midContador3++;
                midContador4++;
            
                  if(alive1 == true && midContador1 >= 400)
                    {
                       midstar1.y += 1;  
                    }
                  
                  if(alive2 == true && midContador2 >= 400)
                    {
                      midstar2.y += 1;
                    }
                    
                    if(alive3 == true && midContador3  >= 400)
                    {
                        midstar3.y += 1;
                    }
                  
                   if(alive4 == true && midContador4  >= 400)
                    {
                        midstar4.y += 1;
                    }
                  
                 
                
                
                if((midstar1.y > fieldRec.y + fieldRec.height))
                {
                    
                    midContador1 = 0;
                    midstar1.y = fieldRec.y  - enemy.height;
                    
                }
                
                 if((midstar2.y > fieldRec.y + fieldRec.height))
                {
                   
                    midContador2 = 0;
                    midstar2.y = fieldRec.y  - enemy.height;
                    
                }
                
                 if((midstar3.y > fieldRec.y + fieldRec.height))
                {
                    midContador3 = 0;
                    midstar3.y = fieldRec.y  - enemy.height;
                }
                
                  if((midstar4.y > fieldRec.y + fieldRec.height))
                {
                    midContador4 = 0;
                    midstar4.y = fieldRec.y  - enemy.height;
                }
                //----------------------------RESPAWN
                
                if(midContador1 >= 750 && alive1 == false)
                {
                    alive1 = true;
                    midstar1.y = fieldRec.y  - enemy.height;
                    
                    midContador1=0;
                }
                
                if(midContador2 >= 750 && alive2 == false)
                {
                    alive2 = true;
                    
                    midstar2.y = fieldRec.y  - enemy.height;
                    
                    midContador2=0;
                }
                
                if(midContador3 >= 750 && alive3 == false)
                {
                    alive3 = true;
                    
                    midstar3.y = fieldRec.y  - enemy.height;
                    midContador3=0;
                }
                
                 if(midContador4 >= 750 && alive4 == false)
                {
                    alive4 = true;
                    
                    midstar4.y = fieldRec.y  - enemy.height;
                    midContador4=0;
                }
                
                
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                
                        if(CheckCollisionCircleRec(ballPosition,ballRadius,player) && ballSpeed.x < 0)
                                {
                                ballPosition.x = player.x + ballRadius + player.width;
                                ballSpeed.x *= -1.03f;
                                PlaySound(star);
                                }
   
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                
                        if(CheckCollisionCircleRec(ballPosition,ballRadius,enemy) && ballSpeed.x > 0)
                                { 
                                ballPosition.x = enemy.x - ballRadius - enemy.width;
                                ballSpeed.x *= -1.03f;
                                PlaySound(darkstar);
                                }
                                
                                
                                
                // Collision with MID STARS (COMETS)
                
                
                if(CheckCollisionCircleRec(ballPosition,ballRadius,midstar1) && ballSpeed.x < 0 && alive1==true)
                                {
                                ballPosition.x = midstar1.x + ballRadius + midstar1.width;
                                ballSpeed.x *= -1.01f;
                                PlaySound(midstar);
                                alive1 = false;
                                midContador1=0;
                                }
                                
                  if(CheckCollisionCircleRec(ballPosition,ballRadius,midstar1) && ballSpeed.x > 0 && alive1==true)
                                {
                                ballPosition.x = midstar1.x - ballRadius - midstar1.width;
                                ballSpeed.x *= -1.01;
                                PlaySound(midstar);
                                alive1 = false;
                                midContador1=0;
                                }    

                  if(CheckCollisionCircleRec(ballPosition,ballRadius,midstar2) && ballSpeed.x < 0 && alive2==true)
                                {
                                ballPosition.x = midstar2.x + ballRadius + midstar2.width;
                                ballSpeed.x *= -1.01f;
                                PlaySound(midstar);
                                alive2 = false;
                                midContador2=0;
                                }
                                
                  if(CheckCollisionCircleRec(ballPosition,ballRadius,midstar2) && ballSpeed.x > 0 && alive2==true)
                                {
                                ballPosition.x = midstar2.x - ballRadius - midstar2.width;
                                ballSpeed.x *= -1.01;
                                PlaySound(midstar);
                                alive2 = false;
                                midContador2=0;
                                } 
                                
                  if(CheckCollisionCircleRec(ballPosition,ballRadius,midstar3) && ballSpeed.x < 0 && alive3==true)
                                {
                                ballPosition.x = midstar3.x + ballRadius + midstar3.width;
                                ballSpeed.x *= -1.01f;
                                PlaySound(midstar);
                                alive3 = false;
                                midContador3=0;
                                }
                                
                  if(CheckCollisionCircleRec(ballPosition,ballRadius,midstar3) && ballSpeed.x > 0 && alive3==true)
                                {
                                ballPosition.x = midstar3.x - ballRadius - midstar3.width;
                                ballSpeed.x *= -1.01;
                                PlaySound(midstar);
                                alive3 = false;
                                midContador3=0;
                                } 
                                
                  if(CheckCollisionCircleRec(ballPosition,ballRadius,midstar4) && ballSpeed.x < 0 && alive4==true)
                                {
                                ballPosition.x = midstar4.x + ballRadius + midstar4.width;
                                ballSpeed.x *= -1.01f;
                                PlaySound(midstar);
                                alive4 = false;
                                midContador4=0;
                                }
                                
                  if(CheckCollisionCircleRec(ballPosition,ballRadius,midstar4) && ballSpeed.x > 0 && alive4==true)
                                {
                                ballPosition.x = midstar4.x - ballRadius - midstar4.width;
                                ballSpeed.x *= -1.01;
                                PlaySound(midstar);
                                alive4 = false;
                                midContador4=0;
                                } 
                
                
                // TODO: Collision detection (ball-limits) logic.....(1p)
                
                //TOP
                
                if((ballPosition.y - ballRadius < fieldRec.y) && ballSpeed.y < 0) 
                {
                    ballSpeed.y *= -1;
                }
                
                //BOT
                
                if((ballPosition.y + ballRadius > fieldRec.y + fieldRec.height) && ballSpeed.y > 0)
                {
                    
                    ballSpeed.y *= -1;                                  
                }
                
                //LEFT
                
                if((ballPosition.x - ballRadius < fieldRec.x) && ballSpeed.x < 0) 
                {
                    ballSpeed.x = 5;  
                    playerLife--;
                    //
                    playerLifeRec.width -= Damage;
                    PlaySound(player_hurt);
                }
                
                //RIGHT
                
                if((ballPosition.x + ballRadius > fieldRec.x + fieldRec.width) && ballSpeed.x > 0) 
                {
                     ballSpeed.x = -5; 
                     enemyLife--;
                     //
                    enemyLifeRec.width -= Damage;
                    enemyLifeRec.x += Damage;
                    PlaySound(enemy_hurt);
                }                                                                       
                
                // TODO: Life bars decrease logic....................(1p)


                // TODO: Time counter logic..........................(0.2p)
                    
                    framesCounter++;
                    if (framesCounter  % 60 == 0)
                    {
                         
                        framesCounter = 0;
                        secondsCounter--;
                        
                    }
                       
                    
                // TODO: Game ending logic...........................(0.2p)
               
                  if(playerLife <= 0 || enemyLife <=0 || secondsCounter <=0)
                  {
                     if(secondsCounter <=0)
                     {
                         if(playerLife < enemyLife) gameResult = 0;
                         else if(playerLife > enemyLife) gameResult = 1;
                         else gameResult = 2;
                     }
                      else if (playerLife <=0) gameResult = 0;
                      else if (enemyLife <= 0) gameResult = 1; 


                        if (gameResult != -1)
                        {
                            screen = ENDING;
                            
                        }
                  }
                
                // TODO: Pause button logic..........................(0.2p)
               } 
                if(IsKeyPressed(KEY_P)){
                    pause= !pause;
                }
                
                if(IsKeyDown(KEY_B) && (pause == true))
                {                   
                        
                         ballPosition = (Vector2){fieldRec.x + fieldRec.width/2, fieldRec.y + fieldRec.height/2};
                         ballSpeed = (Vector2) { GetRandomValue(5,6), GetRandomValue(5,7)};                                                                   //BALL
                            
                        if(GetRandomValue(0,1)) ballSpeed.x *= -1;
                        if(GetRandomValue(0,1)) ballSpeed.y *= -1;
                        
                        player.y = fieldRec.y + fieldRec.height/2 - player.height/2;
                        enemy.y = player.y;
                        
                        
                        
                        midstar1.y = fieldRec.y  - enemy.height;
                        midstar2.y = fieldRec.y  - enemy.height;
                        midstar3.y = fieldRec.y  - enemy.height;
                        midstar4.y = fieldRec.y  - enemy.height;
                        
                        playerLife = 5;
                        enemyLife = 5;
                        
                        alive1 = true;
                        alive2 = true;
                        alive3 = true;
                        alive4 = true;

                        midContador1=0;
                        midContador2=0;
                        midContador3=0;
                        midContador4=0;
                        
                        secondsCounter = 99;
                        framesCounter=0;
                        
                        pause = false;
                        
                        playerLifeRec = playerFillRec;
                        enemyLifeRec = enemyFillRec;
                        gameResult = -1;
                        titleAnimation=-100;
                        screen = TITLE;
                }
                
            } break;
            
            case MULTIPLAYER_GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!

            if (!pause)
            {
                UpdateMusicStream(gameMusic);
                PlayMusicStream (gameMusic);
                
                     // TODO: Ball movement logic.........................(0.2p)
                
                ballPosition.x = ballPosition.x + ballSpeed.x;
                ballPosition.y = ballPosition.y + ballSpeed.y;
                
                // TODO: Player movement logic.......................(0.2p)
                
                    if (IsKeyDown(KEY_W)) player.y -= playerSpeedY;
                    if (IsKeyDown(KEY_S)) player.y += playerSpeedY;
                    
                    
                                
                    if(player.y<fieldRec.y)
                            {
                                player.y = fieldRec.y;
                            }
                            
                    else if(player.y>(fieldRec.y + fieldRec.height - player.height))
                            {
                                player.y = fieldRec.y + fieldRec.height - player.height;
                            }
                    
                    
                // TODO: Enemy movement logic (IA)...................(1p)
                

                if (IsKeyDown(KEY_UP)) enemy.y -= enemySpeedY;
                if (IsKeyDown(KEY_DOWN)) enemy.y += enemySpeedY;
                
                if(enemy.y<fieldRec.y)
                            {
                                enemy.y = fieldRec.y;
                            }   
                    else if(enemy.y>(fieldRec.y + fieldRec.height - enemy.height))
                            {
                                enemy.y = fieldRec.y + fieldRec.height - enemy.height;
                            }
                
                // Mid star movement
                
                
                midContador1++;
                midContador2++;
                midContador3++;
                midContador4++;
            
                  if(alive1 == true && midContador1 >= 400)
                    {
                       midstar1.y += 1;  
                    }
                  
                  if(alive2 == true && midContador2 >= 400)
                    {
                      midstar2.y += 1;
                    }
                    
                    if(alive3 == true && midContador3  >= 400)
                    {
                        midstar3.y += 1;
                    }
                  
                   if(alive4 == true && midContador4  >= 400)
                    {
                        midstar4.y += 1;
                    }
                  
                 
                
                
                if((midstar1.y > fieldRec.y + fieldRec.height))
                {
                    
                    midContador1 = 0;
                    midstar1.y = fieldRec.y  - enemy.height;
                    
                }
                
                 if((midstar2.y > fieldRec.y + fieldRec.height))
                {
                   
                    midContador2 = 0;
                    midstar2.y = fieldRec.y  - enemy.height;
                    
                }
                
                 if((midstar3.y > fieldRec.y + fieldRec.height))
                {
                    midContador3 = 0;
                    midstar3.y = fieldRec.y  - enemy.height;
                }
                
                  if((midstar4.y > fieldRec.y + fieldRec.height))
                {
                    midContador4 = 0;
                    midstar4.y = fieldRec.y  - enemy.height;
                }
                
                if(midContador1 >= 750 && alive1 == false)
                {
                    alive1 = true;
                    midstar1.y = fieldRec.y  - enemy.height;
                    
                    midContador1=0;
                }
                
                if(midContador2 >= 750 && alive2 == false)
                {
                    alive2 = true;
                    
                    midstar2.y = fieldRec.y  - enemy.height;
                    
                    midContador2=0;
                }
                
                if(midContador3 >= 750 && alive3 == false)
                {
                    alive3 = true;
                    
                    midstar3.y = fieldRec.y  - enemy.height;
                    midContador3=0;
                }
                
                 if(midContador4 >= 750 && alive4 == false)
                {
                    alive4 = true;
                    
                    midstar4.y = fieldRec.y  - enemy.height;
                    midContador4=0;
                }
                
                
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                
                        if(CheckCollisionCircleRec(ballPosition,ballRadius,player) && ballSpeed.x < 0)
                                {
                                ballPosition.x = player.x + ballRadius + player.width;
                                ballSpeed.x *= -1.03f;
                                PlaySound(star);
                                }
   
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                
                        if(CheckCollisionCircleRec(ballPosition,ballRadius,enemy) && ballSpeed.x > 0)
                                { 
                                ballPosition.x = enemy.x - ballRadius - enemy.width;
                                ballSpeed.x *= -1.03f;
                                PlaySound(darkstar);
                                }
                                
                                
                                
                // Collision with MID STARS (COMETS)
                
                
                if(CheckCollisionCircleRec(ballPosition,ballRadius,midstar1) && ballSpeed.x < 0 && alive1==true)
                                {
                                ballPosition.x = midstar1.x + ballRadius + midstar1.width;
                                ballSpeed.x *= -1.01f;
                                PlaySound(midstar);
                                alive1 = false;
                                midContador1=0;
                                }
                                
                  if(CheckCollisionCircleRec(ballPosition,ballRadius,midstar1) && ballSpeed.x > 0 && alive1==true)
                                {
                                ballPosition.x = midstar1.x - ballRadius - midstar1.width;
                                ballSpeed.x *= -1.01;
                                PlaySound(midstar);
                                alive1 = false;
                                midContador1=0;
                                }    

                  if(CheckCollisionCircleRec(ballPosition,ballRadius,midstar2) && ballSpeed.x < 0 && alive2==true)
                                {
                                ballPosition.x = midstar2.x + ballRadius + midstar2.width;
                                ballSpeed.x *= -1.01f;
                                PlaySound(midstar);
                                alive2 = false;
                                midContador2=0;
                                }
                                
                  if(CheckCollisionCircleRec(ballPosition,ballRadius,midstar2) && ballSpeed.x > 0 && alive2==true)
                                {
                                ballPosition.x = midstar2.x - ballRadius - midstar2.width;
                                ballSpeed.x *= -1.01;
                                PlaySound(midstar);
                                alive2 = false;
                                midContador2=0;
                                } 
                                
                  if(CheckCollisionCircleRec(ballPosition,ballRadius,midstar3) && ballSpeed.x < 0 && alive3==true)
                                {
                                ballPosition.x = midstar3.x + ballRadius + midstar3.width;
                                ballSpeed.x *= -1.01f;
                                PlaySound(midstar);
                                alive3 = false;
                                midContador3=0;
                                }
                                
                  if(CheckCollisionCircleRec(ballPosition,ballRadius,midstar3) && ballSpeed.x > 0 && alive3==true)
                                {
                                ballPosition.x = midstar3.x - ballRadius - midstar3.width;
                                ballSpeed.x *= -1.01;
                                PlaySound(midstar);
                                alive3 = false;
                                midContador3=0;
                                } 
                                
                  if(CheckCollisionCircleRec(ballPosition,ballRadius,midstar4) && ballSpeed.x < 0 && alive4==true)
                                {
                                ballPosition.x = midstar4.x + ballRadius + midstar4.width;
                                ballSpeed.x *= -1.01f;
                                PlaySound(midstar);
                                alive4 = false;
                                midContador4=0;
                                }
                                
                  if(CheckCollisionCircleRec(ballPosition,ballRadius,midstar4) && ballSpeed.x > 0 && alive4==true)
                                {
                                ballPosition.x = midstar4.x - ballRadius - midstar4.width;
                                ballSpeed.x *= -1.01;
                                PlaySound(midstar);
                                alive4 = false;
                                midContador4=0;
                                } 
                
                
                // TODO: Collision detection (ball-limits) logic.....(1p)
                
                //TOP
                
                if((ballPosition.y - ballRadius < fieldRec.y) && ballSpeed.y < 0) 
                {
                    ballSpeed.y *= -1;
                }
                
                //BOT
                
                if((ballPosition.y + ballRadius > fieldRec.y + fieldRec.height) && ballSpeed.y > 0)
                {
                    
                    ballSpeed.y *= -1;                                  
                }
                
                //LEFT
                
                if((ballPosition.x - ballRadius < fieldRec.x) && ballSpeed.x < 0) 
                {
                    ballSpeed.x = 5;  
                    playerLife--;
                    //
                    playerLifeRec.width -= Damage;
                    PlaySound(player_hurt);
                }
                
                //RIGHT
                
                if((ballPosition.x + ballRadius > fieldRec.x + fieldRec.width) && ballSpeed.x > 0) 
                {
                     ballSpeed.x = -5; 
                     enemyLife--;
                     //
                    enemyLifeRec.width -= Damage;
                    enemyLifeRec.x += Damage;
                    PlaySound(enemy_hurt);
                }                                                                       
                
                // TODO: Life bars decrease logic....................(1p)


                // TODO: Time counter logic..........................(0.2p)
                    
                    framesCounter++;
                    if (framesCounter  % 60 == 0)
                    {
                         
                        framesCounter = 0;
                        secondsCounter--;
                        
                    }
                       
                    
                // TODO: Game ending logic...........................(0.2p)
               
                  if(playerLife <= 0 || enemyLife <=0 || secondsCounter <=0)
                  {
                     if(secondsCounter <=0)
                     {
                         if(playerLife < enemyLife) gameResult = 3;
                         else if(playerLife > enemyLife) gameResult = 4;
                         else gameResult = 2;
                     }
                      else if (playerLife <=0) gameResult = 3;
                      else if (enemyLife <= 0) gameResult = 4; 


                        if (gameResult != -1)
                        {
                            screen = ENDING;
                            
                        }
                  }
                
                // TODO: Pause button logic..........................(0.2p)
               } 
                if(IsKeyPressed(KEY_P)){
                    pause= !pause;
                }
                
                if(IsKeyDown(KEY_B) && (pause == true))
                {                   
                        
                         ballPosition = (Vector2){fieldRec.x + fieldRec.width/2, fieldRec.y + fieldRec.height/2};
                         ballSpeed = (Vector2) { GetRandomValue(5,7), GetRandomValue(5,7)};                                                                   //BALL
                            
                        if(GetRandomValue(0,1)) ballSpeed.x *= -1;
                        if(GetRandomValue(0,1)) ballSpeed.y *= -1;
                        
                        player.y = fieldRec.y + fieldRec.height/2 - player.height/2;
                        enemy.y = player.y;
                        
                        
                        
                        midstar1.y = fieldRec.y  - enemy.height;
                        midstar2.y = fieldRec.y  - enemy.height;
                        midstar3.y = fieldRec.y  - enemy.height;
                        midstar4.y = fieldRec.y  - enemy.height;
                        
                        playerLife = 5;
                        enemyLife = 5;
                        
                        alive1 = true;
                        alive2 = true;
                        alive3 = true;
                        alive4 = true;

                        midContador1=0;
                        midContador2=0;
                        midContador3=0;
                        midContador4=0;
                        
                        secondsCounter = 99;
                        framesCounter=0;
                        
                        pause = false;
                        
                        playerLifeRec = playerFillRec;
                        enemyLifeRec = enemyFillRec;
                        gameResult = -1;
                        titleAnimation=-100;
                        screen = TITLE;
                }
                
                
                
            } break;
            
            case ENDING: 
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................(0.5p)
                
                if (IsKeyPressed(KEY_ENTER))
                {
                        
                        
                        
                         ballPosition = (Vector2){fieldRec.x + fieldRec.width/2, fieldRec.y + fieldRec.height/2};
                         ballSpeed = (Vector2) { GetRandomValue(5,7), GetRandomValue(5,7)};                                                                   //BALL
                            
                        if(GetRandomValue(0,1)) ballSpeed.x *= -1;
                        if(GetRandomValue(0,1)) ballSpeed.y *= -1;
                        
                        player.y = fieldRec.y + fieldRec.height/2 - player.height/2;
                        enemy.y = player.y;
                        
                        
                        
                        midstar1.y = fieldRec.y  - enemy.height;
                        midstar2.y = fieldRec.y  - enemy.height;
                        midstar3.y = fieldRec.y  - enemy.height;
                        midstar4.y = fieldRec.y  - enemy.height;
                        
                        playerLife = 5;
                        enemyLife = 5;
                        
                        alive1 = true;
                        alive2 = true;
                        alive3 = true;
                        alive4 = true;

                        midContador1=0;
                        midContador2=0;
                        midContador3=0;
                        midContador4=0;
                        
                        secondsCounter = 99;
                        framesCounter=0;
                        
                        pause = false;
                        
                        playerLifeRec = playerFillRec;
                        enemyLifeRec = enemyFillRec;
                        gameResult = -1;
                        screen = GAMEPLAY;
                        
                    
                }
                
                if (IsKeyPressed(KEY_M))
                {
                        
                        
                        
                         ballPosition = (Vector2){fieldRec.x + fieldRec.width/2, fieldRec.y + fieldRec.height/2};
                         ballSpeed = (Vector2) { GetRandomValue(5,7), GetRandomValue(5,7)};                                                                  //BALL
                            
                        if(GetRandomValue(0,1)) ballSpeed.x *= -1;
                        if(GetRandomValue(0,1)) ballSpeed.y *= -1;
                        
                        player.y = fieldRec.y + fieldRec.height/2 - player.height/2;
                        enemy.y = player.y;
                        
                        
                        
                        midstar1.y = fieldRec.y  - enemy.height;
                        midstar2.y = fieldRec.y  - enemy.height;
                        midstar3.y = fieldRec.y  - enemy.height;
                        midstar4.y = fieldRec.y  - enemy.height;
                        
                        playerLife = 5;
                        enemyLife = 5;
                        
                        alive1 = true;
                        alive2 = true;
                        alive3 = true;
                        alive4 = true;

                        midContador1=0;
                        midContador2=0;
                        midContador3=0;
                        midContador4=0;
                        
                        secondsCounter = 99;
                        framesCounter=0;
                        
                        pause = false;
                        
                        playerLifeRec = playerFillRec;
                        enemyLifeRec = enemyFillRec;
                        gameResult = -1;
                        screen = MULTIPLAYER_GAMEPLAY;
                        
                    
                }
                
                if(IsKeyDown(KEY_B))
                {                   
                        
                         ballPosition = (Vector2){fieldRec.x + fieldRec.width/2, fieldRec.y + fieldRec.height/2};
                         ballSpeed = (Vector2) { GetRandomValue(5,7), GetRandomValue(5,7)};                                                                     //BALL
                            
                        if(GetRandomValue(0,1)) ballSpeed.x *= -1;
                        if(GetRandomValue(0,1)) ballSpeed.y *= -1;
                        
                        player.y = fieldRec.y + fieldRec.height/2 - player.height/2;
                        enemy.y = player.y;
                        
                        
                        
                        midstar1.y = fieldRec.y  - enemy.height;
                        midstar2.y = fieldRec.y  - enemy.height;
                        midstar3.y = fieldRec.y  - enemy.height;
                        midstar4.y = fieldRec.y  - enemy.height;
                        
                        playerLife = 5;
                        enemyLife = 5;
                        
                        alive1 = true;
                        alive2 = true;
                        alive3 = true;
                        alive4 = true;

                        midContador1=0;
                        midContador2=0;
                        midContador3=0;
                        midContador4=0;
                        
                        secondsCounter = 99;
                        framesCounter=0;
                        
                        pause = false;
                        
                        playerLifeRec = playerFillRec;
                        enemyLifeRec = enemyFillRec;
                        gameResult = -1;
                        titleAnimation=-100;
                        screen = TITLE;
                }
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................(0.2p)
                    DrawTexture(logo, screenWidth/2-logo.width/2, screenHeight/2-logo.height/2,Fade(WHITE,alpha));
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    ClearBackground(BLACK);
                    // TODO: Draw Title..............................(0.2p)
                    DrawText(titleText,screenWidth/2 - MeasureText(titleText,50)/2, titleAnimation, 50, DARKBLUE);
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    
                    if(blink){
                        DrawText(startText1,screenWidth/2 - MeasureText(startText1,20)/2, screenHeight-100, 20, RED);
                        DrawText(startText2,screenWidth/2 - MeasureText(startText2,20)/2, screenHeight-75, 20, RED);
                    }
                    //Decoration
                    
                    
                     DrawTexture(playerstar_end, 50, screenHeight/2-playerstar_end.height/2,WHITE);
                     DrawTexture(darkstar_end, 750-darkstar_end.width, screenHeight/2-darkstar_end.height/2,WHITE);
                     DrawTexture(drawstar_end, screenWidth/2-drawstar_end.width/2, screenHeight/2-darkstar_end.height/2,WHITE);
                   
                    
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    
                   
                    
                    ClearBackground(BLACK);
                    DrawRectangleRec(bgRec, DARKBLUE);                 
                    DrawRectangleRec(fieldRec, BLACK);
                    DrawTexture(background,  bgRec.x + bgMargin, bgRec.y + bgMargin,RAYWHITE);

                    
                    
                    
                    DrawTexture(ball,ballPosition.x-ball.width/2,ballPosition.y-ball.height/2,RAYWHITE);
                    
                    // TODO: Draw player and enemy...................(0.2p)
                    
                    DrawRectangleRec(player,playercolor); //player
            
                    DrawRectangleRec(enemy,enemycolor);  //enemy
                    
                    // Draw middle star
                    
                      if (alive1 == true)
                    {
                        DrawRectangleRec (midstar1,WHITE);
                    }
                    
                    if (alive2 == true)
                    {
                        DrawRectangleRec(midstar2,WHITE);
                    }
                    
                    if (alive3 == true)
                    {
                        DrawRectangleRec (midstar3,WHITE);
                    }
                    
                     if (alive4 == true)
                    {
                        DrawRectangleRec (midstar4,WHITE);
                    }
                    
                    
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    
                    DrawRectangleRec(playerBgRec,BLUE);
                    DrawRectangleRec(playerFillRec,player_life_color);                //PLAYER 
                    DrawRectangleRec(playerLifeRec,playercolor);
                    
                    DrawRectangleRec(enemyBgRec,BLUE);
                    DrawRectangleRec(enemyFillRec,enemy_life_color);                //ENEMY
                    DrawRectangleRec(enemyLifeRec,enemycolor);
                    
                    // TODO: Draw time counter.......................(0.5p)
                    
                    DrawText(FormatText("%i",secondsCounter), screenWidth/2 - MeasureText(FormatText("%i",secondsCounter),40)/2, topMargin - 42, 40, RAYWHITE);
                    
                    // TODO: Draw pause message when required........(0.5p)
                    
                    if(pause){
                    DrawRectangle(0,0,screenWidth,screenHeight, Fade(WHITE,0.8f));
                    DrawText("PAUSE",screenWidth/2 - MeasureText("PAUSE", 30)/2,screenHeight/2-15,30,RED);
                    DrawText("[B] MAIN MENU",screenWidth/2 - MeasureText("[B] MAIN MENU", 20)/2,screenHeight/2+40,20,RED);                    
                    }
                    
                    
                } break;
                
                case MULTIPLAYER_GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                                    
                    ClearBackground(BLACK);
                    DrawRectangleRec(bgRec, DARKBLUE);                 
                    DrawRectangleRec(fieldRec, BLACK);
                    DrawTexture(background,  bgRec.x + bgMargin, bgRec.y + bgMargin,RAYWHITE);
                    
                     

                    
                    DrawTexture(ball,ballPosition.x-ball.width/2,ballPosition.y-ball.height/2,RAYWHITE);
                    
                    // TODO: Draw player and enemy...................(0.2p)
                    
                    DrawRectangleRec(player,playercolor); //player
            
                    DrawRectangleRec(enemy,enemycolor);  //enemy
                    
                    // Draw middle star
                    
                      if (alive1 == true)
                    {
                        DrawRectangleRec (midstar1,WHITE);
                    }
                    
                    if (alive2 == true)
                    {
                        DrawRectangleRec(midstar2,WHITE);
                    }
                    
                    if (alive3 == true)
                    {
                        DrawRectangleRec (midstar3,WHITE);
                    }
                    
                     if (alive4 == true)
                    {
                        DrawRectangleRec (midstar4,WHITE);
                    }
                    
                    
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    
                    DrawRectangleRec(playerBgRec,BLUE);
                    DrawRectangleRec(playerFillRec,player_life_color);                //PLAYER 
                    DrawRectangleRec(playerLifeRec,playercolor);
                    
                    DrawRectangleRec(enemyBgRec,BLUE);
                    DrawRectangleRec(enemyFillRec,enemy_life_color);                //ENEMY
                    DrawRectangleRec(enemyLifeRec,enemycolor);
                    
                    // TODO: Draw time counter.......................(0.5p)
                    
                    DrawText(FormatText("%i",secondsCounter), screenWidth/2 - MeasureText(FormatText("%i",secondsCounter),40)/2, topMargin - 42, 40, RAYWHITE);
                    
                    // TODO: Draw pause message when required........(0.5p)
                    
                    if(pause){
                    DrawRectangle(0,0,screenWidth,screenHeight, Fade(WHITE,0.8f));
                    DrawText("PAUSE",screenWidth/2 - MeasureText("PAUSE", 30)/2,screenHeight/2-15,30,RED);
                    DrawText("[B] MAIN MENU",screenWidth/2 - MeasureText("[B] MAIN MENU", 20)/2,screenHeight/2+40,20,RED);
                    }
                    
                    
                } break;
                case ENDING: 
                {
                    ClearBackground(RAYWHITE);
                    // Draw END screen here!
                    
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    
                   
                        
                    if(gameResult == 0) 
                       {
                           DrawTexture(background_dark, screenWidth/2-background2.width/2, screenHeight/2-background2.height/2,WHITE);
                           DrawText("YOU LOSE", screenWidth/2 - MeasureText("YOU LOSE",40)/2,150,40,enemycolor);
                           DrawTexture(darkstar_end, 50, screenHeight/2-darkstar_end.height/2,WHITE);
                           DrawTexture(darkstar_end, 750-darkstar_end.width, screenHeight/2-darkstar_end.height/2,WHITE);

                       } 
                    
                    else if (gameResult == 1) 
                        
                    {          
                        DrawTexture(background_star, screenWidth/2-background_star.width/2, screenHeight/2-background_star.height/2,WHITE);
                        DrawText("YOU WIN", screenWidth/2 - MeasureText("YOU WIN",40)/2,150,40,playercolor);
                        
                        DrawTexture(playerstar_end, 50, screenHeight/2-playerstar_end.height/2,WHITE);
                        DrawTexture(playerstar_end, 750-playerstar_end.width, screenHeight/2-playerstar_end.height/2,WHITE);
                    }
                    
                    else if (gameResult ==2)
                        
                    {
                        DrawTexture(background_draw, screenWidth/2-background_draw.width/2, screenHeight/2-background_draw.height/2,WHITE);
                        DrawText("DRAW GAME", screenWidth/2 - MeasureText("DRAW GAME",40)/2,150,40,GREEN);    

                        DrawTexture(drawstar_end, 50, screenHeight/2-drawstar_end.height/2,WHITE);
                        DrawTexture(drawstar_end, 750-drawstar_end.width, screenHeight/2-drawstar_end.height/2,WHITE);                        
                    }                       
                    
                      
                        
                   
                        
                    if(gameResult == 3) 
                       {
                           DrawTexture(background_dark, screenWidth/2-background2.width/2, screenHeight/2-background2.height/2,WHITE);
                           DrawText("PLAYER 2 WINS", screenWidth/2 - MeasureText("PLAYER 2 WINS",40)/2,150,40,enemycolor);
                           DrawTexture(darkstar_end, 50, screenHeight/2-darkstar_end.height/2,WHITE);
                           DrawTexture(darkstar_end, 750-darkstar_end.width, screenHeight/2-darkstar_end.height/2,WHITE);

                       } 
                    
                    else if (gameResult == 4) 
                        
                    {          
                        DrawTexture(background_star, screenWidth/2-background_star.width/2, screenHeight/2-background_star.height/2,WHITE);
                        DrawText("PLAYER 1 WINS", screenWidth/2 - MeasureText("PLAYER 1 WINS",40)/2,150,40,playercolor);
                        
                        DrawTexture(playerstar_end, 50, screenHeight/2-playerstar_end.height/2,WHITE);
                        DrawTexture(playerstar_end, 750-playerstar_end.width, screenHeight/2-playerstar_end.height/2,WHITE);
                    }
               
                    
                    DrawText("PRESS ENTER TO PLAY PVE", screenWidth/2 - MeasureText("PRESS ENTER TO PLAY PVE",20)/2,screenHeight - 125,20,SKYBLUE);
                    DrawText("PRESS M TO PLAY PVP", screenWidth/2 - MeasureText("PRESS M TO PLAY PVP",20)/2,screenHeight - 100,20,SKYBLUE);
                    DrawText("PRESS B TO OPEN MENU", screenWidth/2 - MeasureText("PRESS B TO OPEN MENU",20)/2,screenHeight - 75,20,SKYBLUE);    
                    DrawText("PRESS ESC TO EXIT", screenWidth/2 - MeasureText("PRESS ESC TO EXIT",20)/2,screenHeight - 50,20,SKYBLUE);    
   
                    
                    
                
                } break;
                default: break;
            }
        
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }
    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    UnloadTexture(logo);
    UnloadTexture(background);
    UnloadTexture(background2);
    UnloadTexture(playerstar_end);
    UnloadTexture(darkstar_end);
    UnloadTexture(drawstar_end);
    UnloadTexture(ball);
    
    
    UnloadTexture(background_star);
    UnloadTexture(background_dark);
    UnloadTexture(background_draw);
    
    UnloadMusicStream (gameMusic);
    
    UnloadSound (logo_music);
    
    UnloadMusicStream (title_music);
    
    UnloadSound (star);
    UnloadSound (player_hurt);
    
    UnloadSound (darkstar);
    UnloadSound (enemy_hurt);
    
    UnloadSound (midstar);
    
    CloseAudioDevice();
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}